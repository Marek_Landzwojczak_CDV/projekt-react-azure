const TempDbContext = require('./DataAccess/db-context');
const common = require('../common');


module.exports = async function (context, req) {
    await common.functionWrapper(context, req, async (body) => {
        const connectionString = process.env['LogowanieDb'];
        const Dane = new TempDbContext(connectionString, context.log);

        if (req.method === 'PUT') {
            const Temp = req.query['Temp'];
            const Wilg= req.query['Wilg'];
            body.dane = await Dane.UpdataTemp(Temp, Wilg);
            return;
        }

        if (req.method === 'GET') {
            body.dane = await Dane.getTemp();
            return;
        }
    });
};