const sql = require('mssql');

const parser = require('mssql-connection-string');

class TempDbContext {
    constructor(connectionString, log) {
            this.log = log;
            this.config = parser(connectionString);
            this.UpdataTemp = this.UpdataTemp.bind(this);

          
        }
    
        async UpdataTemp(Temp, Wilg) {
            this.log("weszło")
            this.log(Temp)
            this.log(Wilg)
            const connection = await new sql.ConnectionPool(this.config).connect();
            const request = new sql.Request(connection);
            const result = await request.query(`update warunki set Temperatura = '${Temp}', Wilgotnosc = '${Wilg}' where PersonId = 1`);
            this.log("zakoczyło")
            return result.recordset;
        }

        async getTemp() {
            this.log("pobieranie")
            const connection = await new sql.ConnectionPool(this.config).connect();
            const request = new sql.Request(connection);
            const result = await request.query(`select * from warunki`);
            this.log("Pobrane")
            return result.recordset;
        }

    }

    module.exports = TempDbContext;