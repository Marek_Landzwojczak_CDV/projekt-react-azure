const LogowanieDbContext = require('./DataAccess/db-context');
const common = require('../common');


module.exports = async function (context, req) {
    await common.functionWrapper(context, req, async (body) => {
        const connectionString = process.env['LogowanieDb'];
        const peopleDb = new LogowanieDbContext(connectionString, context.log);

        if (req.method === 'POST') {
            const userName = req.query['userName'];
            const password= req.query['password'];
            body.people = await peopleDb.getPeople(userName, password);
            return;
        }

        if (req.method === 'PUT') {
            const Temp = req.query['Temp'];
            const Wilg= req.query['Wilg'];
            body.people = await peopleDb.UpdataTemp(Temp, Wilg);
            return;
        }

        if (req.method === 'GET') {
            body.people = await peopleDb.getTemp();
            return;
        }
    });
}