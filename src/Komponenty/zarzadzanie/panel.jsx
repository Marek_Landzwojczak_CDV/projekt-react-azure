import React, { Component } from 'react';
import Salon from'./salon/salon';
import Kuchnia from './kuchnia/kuchnia';
import Sypialnia from './sypialnia/sypialnia'; 
import Home from './home';
import {BrowserRouter as Router, Link, Route} from 'react-router-dom';

class Panel extends Component {



render(){
    return(<Router>
   
   
    <div className="container">
        <div className="row">
            <div className="col-1">
                <div className="btn-group-vertical">
                       <button className="btn btn-primary btn-lg"><Link to="/" className="link">Start</Link></button>
                       <button className="btn btn-primary btn-lg"><Link to="/salon" className="link">Salon</Link></button>
                       <button className="btn btn-primary btn-lg"><Link to="/kuchnia" className="link">Kuchnia</Link></button>
                       <button className="btn btn-primary btn-lg"><Link to="/sypialnia" className="link">Sypialnia</Link></button>
                </div>  
            </div>
        


          
          <div className="col">
                <Route path="/" exact component={Home} />
                <Route path="/salon"  component={Salon} />
                <Route path="/kuchnia"  component={Kuchnia} />
                <Route path="/sypialnia"  component={Sypialnia} />
                </div>
        </div>
        </div>
        </Router>
    )}
}
export default Panel;